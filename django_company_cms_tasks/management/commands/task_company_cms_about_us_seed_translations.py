from django.core.management.base import BaseCommand

from django_company_cms_tasks.django_company_cms_tasks.tasks import (
    company_cms_about_us_seed_translations,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        company_cms_about_us_seed_translations()
