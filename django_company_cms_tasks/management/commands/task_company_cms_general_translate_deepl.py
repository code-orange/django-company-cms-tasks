from django.core.management.base import BaseCommand

from django_company_cms_tasks.django_company_cms_tasks.tasks import (
    company_cms_general_translate_deepl,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        company_cms_general_translate_deepl()
