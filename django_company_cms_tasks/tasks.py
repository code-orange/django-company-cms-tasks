import deepl
from celery import shared_task
from django.apps import apps
from django.conf import settings

from django_company_cms_about_us.django_company_cms_about_us.models import (
    AboutUsHistory,
)
from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)


@shared_task(name="company_cms_general_seed_translations")
def company_cms_general_seed_translations():
    translate_model = apps.get_model(
        "django_company_cms_general", "CompanyCmsGeneralTranslation"
    )

    for general_content in CompanyCmsGeneral.objects.all():
        for language in settings.LANGUAGES:
            try:
                translation = translate_model.objects.get(
                    master=general_content,
                    language_code=language[0],
                )
            except:
                translation = translate_model(
                    master=general_content,
                    language_code=language[0],
                    content="PLACEHOLDER",
                )
                translation.save()

    return


@shared_task(name="company_cms_general_translate_deepl")
def company_cms_general_translate_deepl():
    translator = deepl.Translator(settings.DEEPL_API_KEY)

    source_lang = "de"

    translate_model = apps.get_model(
        "django_company_cms_general", "CompanyCmsGeneralTranslation"
    )

    for general_content in CompanyCmsGeneral.objects.all():
        try:
            translation_de = translate_model.objects.get(
                master=general_content,
                language_code=source_lang.lower(),
            )
        except:
            continue

        for language in settings.LANGUAGES:
            try:
                translation = translate_model.objects.get(
                    master=general_content,
                    language_code=language[0],
                )
            except:
                continue

            target_lang = language[0].upper()

            # deepl.exceptions.DeepLException: target_lang="EN" is deprecated, please use "EN-GB" or "EN-US" instead.
            if target_lang == "EN":
                target_lang = "EN-US"

            if translation.translation_needed is True:
                translation.content = translator.translate_text(
                    translation_de.content,
                    source_lang=source_lang.upper(),
                    target_lang=target_lang.upper(),
                )
                translation.translation_needed = False

                translation.save()

    return


@shared_task(name="company_cms_about_us_seed_translations")
def company_cms_about_us_seed_translations():
    translate_model = apps.get_model(
        "django_company_cms_about_us", "AboutUsHistoryTranslation"
    )

    for about_us_content in AboutUsHistory.objects.all():
        for language in settings.LANGUAGES:
            try:
                translation = translate_model.objects.get(
                    master=about_us_content,
                    language_code=language[0],
                )
            except:
                translation = translate_model(
                    master=about_us_content,
                    language_code=language[0],
                    content="PLACEHOLDER",
                )
                translation.save()

    return


@shared_task(name="company_cms_about_us_translate_deepl")
def company_cms_about_us_translate_deepl():
    translator = deepl.Translator(settings.DEEPL_API_KEY)

    source_lang = "de"

    translate_model = apps.get_model(
        "django_company_cms_about_us", "AboutUsHistoryTranslation"
    )

    for about_us_content in AboutUsHistory.objects.all():
        try:
            translation_de = translate_model.objects.get(
                master=about_us_content,
                language_code=source_lang.lower(),
            )
        except:
            continue

        for language in settings.LANGUAGES:
            try:
                translation = translate_model.objects.get(
                    master=about_us_content,
                    language_code=language[0],
                )
            except:
                continue

            target_lang = language[0].upper()

            # deepl.exceptions.DeepLException: target_lang="EN" is deprecated, please use "EN-GB" or "EN-US" instead.
            if target_lang == "EN":
                target_lang = "EN-US"

            if translation.translation_needed is True:
                translation.name = translator.translate_text(
                    translation_de.name,
                    source_lang=source_lang.upper(),
                    target_lang=target_lang.upper(),
                )

                translation.description = translator.translate_text(
                    translation_de.description,
                    source_lang=source_lang.upper(),
                    target_lang=target_lang.upper(),
                )

                translation.translation_needed = False

                translation.save()

    return
